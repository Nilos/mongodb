// const express        = require('express');
// const MongoClient    = require('mongodb').MongoClient;
// const bodyParser     = require('body-parser');
// const mydb             = require('./config/db');
//
// const app            = express();
//
// const port = 8000;
//
// app.use(bodyParser.urlencoded({ extended: true }));
//
// MongoClient.connect(mydb.url, (err, database) => {
//     if (err) return console.log(err)
//
//     // Make sure you add the database name and not the collection name
//     db = database.db("carburant");
//     require('./app/routes')(app, db);
//     app.listen(port, () => {
//         console.log('We are live on ' + port);
//     });
// })

var express = require('express');
var mongodb = require("mongodb");
const bodyParser = require('body-parser');
var client = mongodb.MongoClient;
var url = "mongodb://localhost:27017/";
const cors = require('cors');
var app = express();

const dbName = 'Carburant';
var db;

var fs = require('fs'),
    xml2js = require('xml2js');

var parser = new xml2js.Parser();
fs.readFile('D:\\NOSQL\\PrixCarburants.xml', function(err, data) {
    parser.parseString(data, function (err, result) {
        // console.dir(result);
        console.log('Done');
        var test=JSON.stringify(result.pdv_liste.pdv);
        // test.replace("$", "elem");
        test=test.replace(/[$]/g, "elem");
        // console.log(test);
        fs.writeFile('D:\\NOSQL\\carburant.json', test, function (err) {
            if (err) console.log(err);
            console.log('Saved!');
        });
    });



});

let exec = require('child_process').exec
let command = 'mongoimport -d Carburant -c Carbu --file D:\\NOSQL\\carburant.json'
exec(command);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({origin: "http://localhost:4200"}));
client.connect(url, function(err, client) {
    if(err) return console.error(err);

    console.log("Connected successfully to server");

    db = client.db(dbName);


    client.close();
    require('./app/routes')(app, db);

    app.listen(3000);
    console.log("Listening on port 3000");
});