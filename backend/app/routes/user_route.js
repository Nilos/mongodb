var ObjectID = require('mongodb').ObjectID;
var mongodb = require("mongodb");
var client = mongodb.MongoClient;
var url = "mongodb://localhost:27017/";

module.exports = function(app, db) {

    app.get("/list", function(req, res, next) {
        client.connect(url, function (err, client) {

            var db = client.db("Carburant");
            var collection = db.collection("Carbu");

            var query = {};

            var cursor = collection.find(query).toArray();

            cursor.then(function(result) {
                // console.log(result);
                res.json(result);
            })
            // console.log(cursor);

        });
    });

    app.get("/list/:cp", function(req, res, next) {
        client.connect(url, function (err, client) {

            var cp = req.params.cp;
            var db = client.db("Carburant");
            var collection = db.collection("Carbu");

            var query = {
                "elem.cp": cp
            };

            var cursor = collection.find(query).toArray();

            cursor.then(function(result) {
                // console.log(result);
                res.json(result);
            })
            // console.log(cursor);


        });
    });

    app.get('/user/:id', (req, res) => {
        const id = req.params.id;
        const details = { '_id': new ObjectID(id) };
        client.connect(url, function (err, client) {
            var db = client.db("Carburant");
            db.collection('users').findOne(details, (err, item) => {
                if (err) {
                    res.send({'error': err});
                } else {
                    res.send(item);
                }
            });
        });
    });

        app.post('/register', (req, res) => {
            console.log(req.body);
            const user = { email: req.body.email, password: req.body.password };
            client.connect(url, function (err, client) {
                var db = client.db("Carburant");
                db.collection('users').insert(user, (err, result) => {
                    if (err) {
                        res.send({'error': err.message});
                    } else {
                        res.send(result.ops[0]);
                    }
                });
            });
        });

    app.put('/user/:id', (req, res) => {
        const id = req.params.id;
        const details = { '_id': new ObjectID(id) };
        const note = { email: req.body.email, password: req.body.password };
        client.connect(url, function (err, client) {
            var db = client.db("Carburant");
            db.collection('users').update(details, note, (err, result) => {
                if (err) {
                    res.send({'error': 'An error has occurred'});
                } else {
                    res.send(note);
                }
            });
        });
    });

    app.delete('/user/:id', (req, res) => {
        const id = req.params.id;
        const details = { '_id': new ObjectID(id) };
        client.connect(url, function (err, client) {
            var db = client.db("Carburant");
            db.collection('users').remove(details, (err, item) => {
                if (err) {
                    res.send({'error': 'An error has occurred'});
                } else {
                    res.send('Note ' + id + ' deleted!');
                }
            });
        });
    });
};