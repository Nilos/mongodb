export class Station {
    id: string;
    latitude: number;
    longitude: number;
    cp: string;
    pop: string;
}