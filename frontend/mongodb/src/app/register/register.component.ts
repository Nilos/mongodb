import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  create_user(email: string, password: string): void {

      email = email.trim();
      password = password.trim();
      console.log(email);
      if (!email) { return; }
      if (!password) { return; }

      const body = JSON.stringify({
          email: email,
          password: password
      });

      // http :
      const req = this.http.post('http://localhost:3000/register', body).subscribe(
              res => {
                  console.log(res.valueOf());
              },
              err => {
                  console.log('Error occured');
              }
          );
  }

}
