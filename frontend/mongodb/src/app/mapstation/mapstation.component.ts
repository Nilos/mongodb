import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as L from 'leaflet';

declare var require: any;

@Component({
  selector: 'app-mapstation',
  templateUrl: './mapstation.component.html',
  styleUrls: ['./mapstation.component.css']
})
export class MapstationComponent implements OnInit {

    code_postal = 75001;
    message = "";
    data = "";
    private myfrugalmap;
    array_markers = [];

    constructor(private http: HttpClient) {
    }
    // Fonction d'initialisation du composant.
    ngOnInit() {

      // Déclaration de la carte avec les coordonnées du centre et le niveau de zoom.
      this.myfrugalmap = L.map('frugalmap').setView([48.8566, 2.3522], 12);

      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: 'Frugal Map'
      }).addTo(this.myfrugalmap);

      const myIcon = L.icon({
        iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon.png'
      });

        this.http.get('http://localhost:3000/list/75001').subscribe((data: any) => {
          this.data=data;
            data.forEach(podotactile => {
              const lat = podotactile.elem.latitude / 100000;
              const lng = podotactile.elem.longitude / 100000;
              let marker = L.marker([lat, lng], {icon: myIcon}).bindPopup(podotactile.adresse[0]).addTo(this.myfrugalmap);
              this.array_markers.push(marker)
            });
        });
    }

  update_cp(){

      for (let i = 0; i < this.array_markers.length; i++){
        this.myfrugalmap.removeLayer(this.array_markers[i]);
      }
      this.array_markers = [];

      const myIcon = L.icon({
        iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon.png'
      });

      this.http.get('http://localhost:3000/list/'+this.code_postal.valueOf()).subscribe((data: any) => {
        this.data=data;
        data.forEach(podotactile => {
          const lat = podotactile.elem.latitude / 100000;
          const lng = podotactile.elem.longitude / 100000;
          let marker = L.marker([lat, lng], {icon: myIcon}).bindPopup(podotactile.adresse[0]).addTo(this.myfrugalmap);
          this.array_markers.push(marker);
        });
        if(data.length == 0){
          this.message = "Aucun résultat";
        }
        else{
          this.message = "";
        }
      });
  }

}
