import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapstationComponent } from './mapstation.component';

describe('MapstationComponent', () => {
  let component: MapstationComponent;
  let fixture: ComponentFixture<MapstationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapstationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapstationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
