import {Component, Input, OnInit} from '@angular/core';
import { Station } from '../station';

@Component({
  selector: 'app-stations',
  templateUrl: './stations.component.html',
  styleUrls: ['./stations.component.css']
})
export class StationsComponent implements OnInit {

    @Input() station: object;

    constructor() { }

    ngOnInit() {
    }

}
