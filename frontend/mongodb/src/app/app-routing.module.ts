import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticateComponent } from './authenticate/authenticate.component';
import { MapstationComponent } from './mapstation/mapstation.component';
import {RegisterComponent} from './register/register.component';

const routes: Routes = [
    { path: 'authenticate', component: AuthenticateComponent },
    { path: 'register', component: RegisterComponent },
    { path: '', component: MapstationComponent },
];

@NgModule({
    imports: [
        [ RouterModule.forRoot(routes) ],
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
